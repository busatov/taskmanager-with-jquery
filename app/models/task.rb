class Task < ActiveRecord::Base

	belongs_to :project

	default_scope { order(:priority) }

	validates :name, presence: { message: 'Please provide a name' }
	validates_presence_of :project

end
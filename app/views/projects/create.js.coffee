<% if @project.valid? %>

$('.projects').children('.row').children('.project-create').append '<%= j(render @project) %></li>'

<% else %>

$('.nav.nav-tabs input').val ''
$('.nav.nav-tabs input').attr 'placeholder', '<%= @project.errors[:name].first %>'

<% end %>
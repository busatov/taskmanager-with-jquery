<% if @task.valid? %>

$('.project.project-<%=@task.project_id%>').children().children('.panel-body').children('.taskpane').append '<%= j(render @task) %>'
$('input.add-task-form').val ''

<% else %>

$('.task-create #name').val ''
$('.task-create #name').attr 'placeholder', '<%= @task.errors[:name].first %>'

<% end %>
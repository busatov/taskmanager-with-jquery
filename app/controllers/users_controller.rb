class UsersController < ApplicationController
  def new
  	@user = User.new
  	render :sign_up
  end

  def create
  	@user = User.new(user_params)
    @projects = Project.all
  	if @user.save
  		sign_in @user
  		flash[:success] = "Welcome!"
      render :show
  	end
  end
  
  def show
  	@user = User.find(params[:id])
  	@projects = Project.all
    @project = Project.first
  end

  def index
    if signed_in?
      @user = User.find(current_user.id)
      @projects = Project.all
      @project = Project.first
    end
  end

  def user_params
	params.permit(:name, :email, :password, :password_confirmation)
  end
end

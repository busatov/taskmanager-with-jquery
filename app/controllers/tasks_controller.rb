class TasksController < ApplicationController

  def create
    if params[:name].length>27
      abort("too long")
    end
    if !Project.find(params[:project_id]).tasks.first
      @task = Project.find(params[:project_id]).tasks.create(name: params[:name], priority: 1)
    else
      new_priority = Project.find(params[:project_id]).tasks.maximum(:priority) + 1
      @task = Project.find(params[:project_id]).tasks.create(name: params[:name], priority: new_priority)
    end
  end

  def destroy
    @task = Task.find(params[:id])
    @task.destroy
  end

  def change_up_priority
    @task_current = Task.find_by(id: params[:id])
    if @task_current.priority != 1
      @task_next = Task.find_by(priority: @task_current.priority.to_i + params[:priority].to_i, project_id: @task_current.project_id)
      @task_current.decrement!(:priority)
      @task_next.increment!(:priority)
      @task_current.save
      @task_next.save
    end
  end

  def change_down_priority
    @task_current = Task.find_by(id: params[:id])
    project_current = Project.find_by(id: @task_current.project_id)
    @max = project_current.tasks.maximum("priority")
    if @task_current.priority != @max
      @task_next = Task.find_by(priority: @task_current.priority.to_i + params[:priority].to_i, project_id: @task_current.project_id)
      @task_current.increment!(:priority)
      @task_next.decrement!(:priority)
      @task_current.save
      @task_next.save
    end
  end
end

class ProjectsController < ApplicationController

	def index
		@projects = Projects.all
		@project = Project.first
	end

	def show
		@project = Project.find(params[:id])
	end

	def create
		@project = Project.create(name: params[:name], user_id: current_user.id)
    	@projects = Project.all
  	end

  	def destroy
   	 	@deleted = Project.find(params[:id]).destroy
   	 	@project = Project.first
    	render :show
 	end
end
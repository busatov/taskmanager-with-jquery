function changePriorityDown (current_task_priority, current_task_project)
{
  if (!$(this).is(':last-child')){
    next_task_priority = current_task_priority + 1
    $( '.project-' + current_task_project + '.priority-' + next_task_priority).hide (0, function() {
      $('.project-' + current_task_project + '.priority-' + current_task_priority).insertAfter ('.project-' + current_task_project + '.priority-' + next_task_priority)
      $('.project-' + current_task_project + '.priority-' + current_task_priority).toggleClass('project-' + current_task_project + ' priority-' + current_task_priority + ' temp')
      $('.project-' + current_task_project + '.priority-' + next_task_priority).toggleClass('project-' + current_task_project + ' priority-' + next_task_priority + ' project-' + current_task_project + ' priority-' + current_task_priority)
      $('.temp').toggleClass('temp project-' + current_task_project + ' priority-' + next_task_priority)
      $('.project-' + current_task_project + '.priority-' + current_task_priority).show ()
      $('.project-' + current_task_project + '.priority-' + current_task_priority).find('.down').attr("onclick", "return changePriorityDown(" + current_task_priority + ", " + current_task_project + ");")
      $('.project-' + current_task_project + '.priority-' + next_task_priority).find('.down').attr("onclick", "return changePriorityDown(" + next_task_priority + ", " + current_task_project + ");")
      $('.project-' + current_task_project + '.priority-' + current_task_priority).find('.up').attr("onclick", "return changePriorityUp(" + current_task_priority + ", " + current_task_project + ");")
      $('.project-' + current_task_project + '.priority-' + next_task_priority).find('.up').attr("onclick", "return changePriorityUp(" + next_task_priority + ", " + current_task_project + ");")
})
}
}

function changePriorityUp (current_task_priority, current_task_project)
{
  if (current_task_priority != 1){
    next_task_priority = current_task_priority - 1
    $('.project-' + current_task_project + '.priority-' + next_task_priority).hide (0, function() {
      $('.project-' + current_task_project + '.priority-' + current_task_priority).insertBefore ('.project-' + current_task_project + '.priority-' + next_task_priority)
      $('.project-' + current_task_project + '.priority-' + current_task_priority).toggleClass('project-' + current_task_project + ' priority-' + current_task_priority + ' temp')
      $('.project-' + current_task_project + '.priority-' + next_task_priority).toggleClass('project-' + current_task_project +' priority-' + next_task_priority + ' project-' + current_task_project +' priority-' + current_task_priority)
      $('.temp').toggleClass('temp project-' + current_task_project + ' priority-' + next_task_priority)
      $('.project-' + current_task_project + '.priority-' + current_task_priority).show ()
      $('.project-' + current_task_project + '.priority-' + current_task_priority).find('.down').attr("onclick", "return changePriorityDown(" + current_task_priority + ", " + current_task_project + ");")
      $('.project-' + current_task_project + '.priority-' + next_task_priority).find('.down').attr("onclick", "return changePriorityDown(" + next_task_priority  + ", " + current_task_project + ");")
      $('.project-' + current_task_project + '.priority-' + current_task_priority).find('.up').attr("onclick", "return changePriorityUp(" + current_task_priority  + ", " + current_task_project + ");")
      $('.project-' + current_task_project + '.priority-' + next_task_priority).find('.up').attr("onclick", "return changePriorityUp(" + next_task_priority  + ", " + current_task_project + ");")
})
}
}

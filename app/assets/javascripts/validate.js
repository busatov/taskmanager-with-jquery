function validateSignInForm()
{
var x=document.forms["sign-in_form"]["email"].value;
var atpos=x.indexOf("@");
var dotpos=x.lastIndexOf(".");
alert = "<div class=\"alert alert-dismissable alert-danger\">"
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
  {
  alert += "Not a valid e-mail address <br />";
  }

var p=document.forms["sign-in_form"]["password"].value;
if (p.length < 6 )
  {
  alert+="Password is too short";
  }

if (alert != "<div class=\"alert alert-dismissable alert-danger\">")
  {
  	alert+="</div>"
  	bootbox.alert(alert);
  	return false;
  }
}

function validateSignUpForm()
{
alert = "<div class=\"alert alert-dismissable alert-danger\">"
var n=document.forms["sign-up_form"]["name"].value;
if (n==null || n=="" || n==" ")
  {
  alert+="Name is too short <br />";
  }
var x=document.forms["sign-up_form"]["email"].value;
var atpos=x.indexOf("@");
var dotpos=x.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
  {
  alert += "Not a valid e-mail address <br />";
  }
var p=document.forms["sign-up_form"]["password"].value;
if (p.length<6)
  {
  alert+="password is too short <br />";
  }
var pc=document.forms["sign-up_form"]["password_confirmation"].value;
if (p!=pc)
  {
  	alert+="invalid password match <br />"
  }
if (alert != "<div class=\"alert alert-dismissable alert-danger\">")
  {
  	alert+="</div>"
  	bootbox.alert(alert);
  	return false;
  }
}